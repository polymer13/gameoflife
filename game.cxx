#include "game.hxx"
#include <iostream>

using namespace game;
using GEC = Game::Error::Code;

Game::Error::Error (GEC c, std::string msg, int lnum, const char* fname):
  code(c), message(msg), line_number(lnum),filename(fname)
{
}

Game::Game(Settings s): m_window(s),
                        m_renderer(m_window),
                        m_grid({50,50}, {0, 0, s.window_resolution[0], s.window_resolution[1]}),
                        m_settings(s),
                        m_game_pause_mode_active(false)
{
}

Game::~Game()
{
  SDL_Quit();
}

void Game::init()
{
}

void Game::run()
{
  auto& res = m_settings.window_resolution;

  bool keep_going = true;

  while(keep_going) {

    SDL_Event event;

    m_renderer.set_draw_color({64, 0, 64, 255});
    m_renderer.clear();

    m_renderer.set_draw_color({0, 255, 255, 255});
    m_grid.draw(m_renderer);

    game_update();

    while(SDL_PollEvent(&event)) {
      switch(event.type) {
      case SDL_QUIT:
        keep_going = false;
        break;
      case SDL_MOUSEBUTTONDOWN:
        if(event.button.button == SDL_BUTTON_LEFT) {
          // Fill Cell
          m_grid.report_select({event.button.x, event.button.y});
        }
        else if (event.button.button == SDL_BUTTON_RIGHT) {
          // Clear cell
          m_grid.report_unselect({event.button.x, event.button.y});
        }
        break;
      case SDL_KEYUP:
        std::cout << "key up\n";
        switch (event.key.keysym.scancode) {
          case SDL_SCANCODE_SPACE:
            m_game_pause_mode_active = !m_game_pause_mode_active;
            std::cout << "game_pause: " << m_game_pause_mode_active << '\n';
        default:
          break;
        }
        break;
      }
    }

    m_renderer.show();
    SDL_Delay(50);
  }

}

void Game::exit()
{
}

Game::Renderer::Renderer(Window& win)
{
  sdl_renderer = SDL_CreateRenderer(win.get_sdl_window(), -1, SDL_RENDERER_ACCELERATED);

  if(sdl_renderer == nullptr) {
    throw GAME_ERROR(GFX_RENDERER_CONTEXT_INIT_FAULT, "Could not initialize gfx renderer context!");
  }
}

Game::Renderer::~Renderer()
{
}

void Game::Renderer::draw_line(geom::ILine line, int thickness)
{
  SDL_RenderDrawLine(sdl_renderer, line.start.x, line.start.y, line.end.x, line.end.y);
}

void Game::Renderer::draw_rect(geom::IRect rect)
{
  SDL_RenderDrawRect(sdl_renderer, (SDL_Rect*)&rect);
}

void Game::Renderer::fill_rect(geom::IRect rect)
{
  SDL_RenderFillRect(sdl_renderer, (SDL_Rect*)&rect);
}

void Game::Renderer::set_draw_color(draw::RgbaColor col)
{
  SDL_SetRenderDrawColor(sdl_renderer, col.r, col.g, col.b, col.a);
}

void Game::Renderer::clear()
{
  SDL_RenderClear(sdl_renderer);
}

void Game::Renderer::show()
{
  SDL_RenderPresent(sdl_renderer);
}

Game::Window::Window(Settings& settings)
{
  sdl_window = SDL_CreateWindow("The Game Of Life", 0, 0, settings.window_resolution[0], settings.window_resolution[1], SDL_WINDOW_SHOWN);

  if(sdl_window == nullptr) {
    throw GAME_ERROR(GFX_CONTEXT_INIT_FAULT, "Could not initlize gfx context!");
  }
}

Game::Window::~Window()
{
  SDL_DestroyWindow(sdl_window);
}

SDL_Renderer* Game::Renderer::get_sdl_renderer()
{
  return sdl_renderer;
}

SDL_Window* Game::Window::get_sdl_window()
{
  return sdl_window;
}

Game::Grid::Grid(): Drawable<Game::Renderer>({0,0,100,100}), m_cell_dims({10, 10}),  m_cells(11, 11)
{
}

Game::Grid::Grid(geom::IVec2 cell_dims, geom::IRect area_rect): Drawable<Game::Renderer>(area_rect),
                                                                m_cell_dims(cell_dims),
                                                                m_cells(area_rect.width/cell_dims.x,area_rect.height/cell_dims.y)
{
}

Game::Grid::~Grid()
{
}

void Game::Grid::alloc()
{
}

void Game::Grid::clear()
{
}

void Game::Grid::set_cell_dimensions(geom::IVec2 nd)
{
  m_cell_dims = nd;
}

void Game::Grid::draw(Game::Renderer& rend)
{
  auto reg_rect = get_region_rect();

  int row_size = reg_rect.width / m_cell_dims.x;

  rend.set_draw_color({211, 211, 0, 255});

  for(int i = 0; i < m_cells.cell_count(); i++) {
    if(m_cells.get_state(i)) {
      int column = i % row_size;
      int row = i / row_size;
      rend.fill_rect({column*m_cell_dims.x, row*m_cell_dims.y, m_cell_dims.x, m_cell_dims.y});
    }
  }

  rend.set_draw_color({0, 255, 255, 255});

  for(int x = 0; x < (reg_rect.width / m_cell_dims.x)+1; x++) {
    rend.draw_line({{ x*m_cell_dims.x, 0 }, { x*m_cell_dims.x, reg_rect.height }}, 10);
  }

  for(int y = 0; y < (reg_rect.height / m_cell_dims.y)+1; y++) {
    rend.draw_line({{ 0, y*m_cell_dims.y }, { reg_rect.width, y*m_cell_dims.y }}, 10);
  }

}

void Game::Grid::report_select(geom::IVec2 click_loc)
{
  int cell_num = cell_index_from_location(click_loc);
  if(cell_num != -1) {
    m_cells.set_state(cell_num, true);
  }
}

void Game::Grid::report_unselect(geom::IVec2 click_loc)
{
  int cell_num = cell_index_from_location(click_loc);
  if(cell_num != -1) {
    m_cells.set_state(cell_num, false);
  }
}

int Game::Grid::cell_index_from_location(geom::IVec2 loc)
{
  auto reg_rect = get_region_rect();
  loc.x -= reg_rect.x;
  loc.y -= reg_rect.y;

  if(loc.x > reg_rect.width) {
    return -1;
  }
  else if (loc.y > reg_rect.height) {
    return -1;
  }

  return (loc.x / m_cell_dims.x) + (loc.y / m_cell_dims.y) * (reg_rect.width / m_cell_dims.x);
}

int Game::Grid::row_size() {
  return m_cells.get_width();
}

int Game::Grid::column_size() {
  return m_cells.get_height();
}

int Game::count_number_of_active_neightboors(int cell_index) {

  int row_size = m_grid.row_size();
  int column_size = m_grid.column_size();

  auto right_cell = [=]() -> int {
    return  m_grid.get_cell_state(cell_index + 1);
  };

  auto left_cell = [=]() -> int {
    return m_grid.get_cell_state( cell_index - 1 );
  };

  auto bottom_cell = [=]() -> int {
    return m_grid.get_cell_state( cell_index + row_size );
  };

  auto top_cell = [=]() -> int {
    return m_grid.get_cell_state( cell_index - row_size );
  };

  auto bottom_right_cell = [=]() -> int {
    return m_grid.get_cell_state( cell_index + row_size + 1 );
  };

  auto bottom_left_cell = [=]() -> int {
    return m_grid.get_cell_state( cell_index + row_size - 1 );
  };

  auto top_right_cell = [=]() -> int {
    return m_grid.get_cell_state( cell_index - row_size + 1 );
  };

  auto top_left_cell = [=]() -> int {
    return m_grid.get_cell_state( cell_index - row_size - 1 );
  };

  if((cell_index % row_size) == 0 /*left edging*/) {
    if(cell_index < row_size /*top edging*/) {
      return right_cell() + bottom_cell() + bottom_right_cell();
    }
    else if ((cell_index / row_size) >= (column_size-1) /*bottom edging*/) {
      return right_cell() + top_cell() + top_right_cell();
    }
    else { /*only left edging*/
      return right_cell() + top_cell() + top_right_cell() + bottom_cell() + bottom_right_cell();
    }
  }
  else if((cell_index % row_size) == (row_size-1)/*right edging*/) {
    if(cell_index < row_size /*top edging*/) {
      return left_cell() + bottom_cell() + bottom_left_cell();
    }
    else if ((cell_index / row_size) >= (column_size-1) /*bottom edging*/) {
      return left_cell() + top_cell() + top_left_cell();
    }
    else { /*only right edging*/
      return left_cell() + top_cell() + top_left_cell() + bottom_cell() + bottom_left_cell();
    }
  }
  else {
    if(cell_index < row_size /*top edging*/) {
      return right_cell() + bottom_cell() + bottom_right_cell() + bottom_left_cell() + left_cell();
    }
    else if ((cell_index / row_size) >= (column_size-1) /*bottom edging*/) {
      return right_cell() + top_cell() + top_right_cell() + top_left_cell() + left_cell();
    }
    else { /* free cell */
      return right_cell() + top_cell() + top_right_cell() + top_left_cell() + left_cell() + bottom_cell() + bottom_right_cell() + bottom_left_cell();
    }
  }

  return 0;
}

void Game::game_update() {

  m_grid.update();
  if( m_game_pause_mode_active ) {
    return;
  }
  // game.swap_buffers();
  for(int i = 0; i < m_grid.get_number_of_cells(); i++) {
    int active_neightboor_count = count_number_of_active_neightboors(i);
    if(m_grid.get_cell_state(i) == true && active_neightboor_count < 4 && active_neightboor_count > 1) {
      continue; // stay alive
    }
    else {
      if(active_neightboor_count == 3) {
        m_grid.set_cell_state(i, true); // comes alive
      }
      else {
        m_grid.set_cell_state(i, false); // cell dies or stays dead
      }
    }

  }
}

void Game::Grid::update()
{
  m_cells.sync_buffers();
}

void Game::Grid::set_cell_state(int idx, bool state)
{
  m_cells.set_state(idx, state);
}

int Game::Grid::get_cell_state(int idx) const
{
  return m_cells.get_state(idx);
}

int Game::Grid::get_number_of_cells() const
{
  return m_cells.cell_count();
}

Game::Grid::Cells::Cells(int w, int h): m_width(w), m_height(h),
                                        m_cell_count(w*h),
                                        m_rootbuffer_size(0),
                                        m_subbuffer_size(0),
                                        m_root_buffer(nullptr),
                                        m_write_buffer(nullptr),
                                        m_read_buffer(nullptr)
{
  m_subbuffer_size = (m_cell_count / 32) + !!(m_cell_count % 32) * 1;
  m_rootbuffer_size = m_subbuffer_size * 2;

  m_root_buffer = new uint32_t[m_rootbuffer_size];
  m_write_buffer = m_root_buffer;
  m_read_buffer= m_write_buffer + m_subbuffer_size;

  std::fill(m_root_buffer, m_root_buffer+m_rootbuffer_size, 0);

  std::cout << "Width: " << m_width << '\n'
            << "Height: " << m_height << '\n'
            << "Cell count: " << m_cell_count << '\n';
}

Game::Grid::Cells::~Cells()
{
  delete[] m_root_buffer;
}

void Game::Grid::Cells::swap_buffers()
{
  std::swap(m_write_buffer, m_read_buffer);
}

void Game::Grid::Cells::sync_buffers()
{
  std::copy(m_write_buffer, m_read_buffer, m_read_buffer);
}

bool Game::Grid::Cells::get_state(int idx) const
{
  if(idx >= 300) {
    std::cout << idx << '\n';
  }
  // std::cout << "Cell state idx: " << idx << '\n';
  if(idx >= m_cell_count) {
    throw GAME_ERROR(GAME_CELL_OUT_OF_BOUND_INDEX,
                     "Tried to access grid cell with an out of bounds index while requesting cell state!");
  }

  return m_read_buffer[idx/32] & (1 << idx % 32);
}

void Game::Grid::Cells::set_state(int idx, bool state)
{
  if(idx >= m_cell_count) {
    throw GAME_ERROR(GAME_CELL_OUT_OF_BOUND_INDEX,
                     "Tried to access grid cell with an out of bounds index while setting cell state!");
  }

  uint32_t bit = 0x1 << (idx % 32);
  uint32_t bits = m_write_buffer[idx/32];

  m_write_buffer[idx/32] = ((bits | bit) * state) + (~(~bits | bit) * !state);
}

// Game::Grid::Cells::
