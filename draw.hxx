#ifndef DRAW_HEADER_HXX
#define DRAW_HEADER_HXX
#include "geom.hxx"
#include <stdint.h>
#include <vector>

namespace draw {

  struct RgbaColor {
    unsigned char r, g, b, a;
  };

  template<typename ColorFormat>
  struct PixelRenderer {
    using Rect = geom::IRect;
    using Line = geom::ILine;
    virtual void set_draw_color(ColorFormat col) = 0;
    virtual void clear() = 0;
    virtual void draw_line(Line, int thickness) = 0;
    virtual void draw_rect(Rect) = 0;
    virtual void fill_rect(Rect) = 0;
    virtual void show() = 0;
  };

  template<typename Renderer>
  class Drawable {
  public:

    using Rect = typename Renderer::Rect;

    virtual void draw(Renderer& rend) = 0;

    Drawable(): m_region_rect() {}
    Drawable(Rect rr): m_region_rect(rr) {}

    Rect get_region_rect() {
      return m_region_rect;
    }

  private:
    Rect m_region_rect;
  };



}
#endif //DRAW_HEADER_HXX
