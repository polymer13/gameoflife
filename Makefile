##
# @project gameoflife
#
# @file
# @version 0.1

CXX=g++

gameoflife: main.cxx game.cxx draw.cxx
	${CXX} -o $@ $^ -lSDL2 -g

# end
