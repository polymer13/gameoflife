#ifndef GAME_HEADER_HXX
#define GAME_HEADER_HXX
#include <SDL2/SDL.h>
#include <string>
#include <vector>
#include "draw.hxx"

namespace game {

  class Game {
  public:

    struct Settings
    {
      int window_resolution[2];
      int cell_size;
    };

    struct Error
    {
      enum Code {
        GFX_CONTEXT_INIT_FAULT,
        GFX_RENDERER_CONTEXT_INIT_FAULT,
        GAME_CELL_OUT_OF_BOUND_INDEX
      } code;

      std::string message;
      int line_number;
      const char* filename;

      explicit Error(Error::Code c, std::string msg, int line_number, const char* fname);
    };

    #define GAME_ERROR(err_code, err_msg) game::Game::Error(game::Game::Error::Code::err_code, err_msg, __LINE__, __FILE__)

    explicit Game(Settings s);
    ~Game();

    void init();
    void run();
    void exit();
  private:

    void game_update();
    int count_number_of_active_neightboors(int cell_index);

    struct Window {
    public:

      Window(Settings& settings);
      ~Window();

      SDL_Window* get_sdl_window();

    private:
      SDL_Window *sdl_window;
    } m_window;

    struct Renderer: public draw::PixelRenderer<draw::RgbaColor> {
    public:

      explicit Renderer(Game::Window& win);
      ~Renderer();

      void set_draw_color(draw::RgbaColor col) override;
      void clear() override;
      void draw_line(geom::ILine line, int thickness) override;
      void draw_rect(geom::IRect rect) override;
      void fill_rect(geom::IRect rect) override;
      void show() override;

      SDL_Renderer* get_sdl_renderer();

    private:
      SDL_Renderer *sdl_renderer;
      draw::RgbaColor draw_color;
    } m_renderer;


    class Grid: public draw::Drawable<Game::Renderer> {
    public:

      void alloc();
      void clear();
      void update();
      void set_cell_dimensions(geom::IVec2 nd);

      void draw(Game::Renderer& rend) override;
      void report_select(geom::IVec2 click_loc);
      void report_unselect(geom::IVec2 click_loc);
      int cell_index_from_location(geom::IVec2 loc);

      int row_size();
      int column_size();
      void set_cell_state(int idx, bool state);
      int get_cell_state(int idx) const;
      int get_number_of_cells() const;

      Grid();
      Grid(geom::IVec2 cell_dims, geom::IRect area_rect);
      ~Grid();

    private:
      geom::IVec2 m_cell_dims;
      // std::vector<bool> m_cells;

      struct Cells {
      public:

        Cells(int w, int h);
        ~Cells();
        void swap_buffers();
        void sync_buffers();
        bool get_state(int idx) const;
        void set_state(int idx, bool state);
        int cell_count() const { return m_cell_count; }
        int get_width() const { return m_width; }
        int get_height() const { return m_height; }

      private:
        int m_width, m_height, m_cell_count, m_rootbuffer_size, m_subbuffer_size;
        uint32_t *m_root_buffer;
        uint32_t *m_write_buffer;
        uint32_t *m_read_buffer;
      } m_cells;

    } m_grid;

    Settings m_settings;
    bool m_game_pause_mode_active;
  };

}

#endif //GAME_HEADER_HXX
