#include <iostream>
#include "game.hxx"


int main (int argc, char** argv)
{

  if(SDL_Init(SDL_INIT_VIDEO) != 0) {
    std::cout << "Could not initialize SDL2!\n";
    return 1;
  }

  game::Game game ({{1024, 786}, 25});

  try {
    game.init();
    game.run();
  } catch( game::Game::Error ge ) {
    std::cout << "Exception Thrown:\n" << ge.filename << ":" << ge.line_number << " " << ge.message << '\n';
  }

  game.exit();

  std::cout << "Hello world!\n";
  return 0;
}
