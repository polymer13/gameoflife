#ifndef GEOM_HEADER_HXX
#define GEOM_HEADER_HXX

namespace geom {

  template<typename Unit>
  struct Rect {
    Unit x,y, width, height;
  };

  template<typename Unit>
  struct Vec2 {
    Unit x, y;

    static Vec2<Unit> Zero() {
      return Vec2<Unit> {0,0};
    }

    Vec2 operator+(const Vec2& rhs) {
      return Vec2(x + rhs.x, y + rhs.y);
    }

    Vec2 operator-(const Vec2& rhs) {
      return Vec2(x - rhs.x, y - rhs.y);
    }

  };


  template<typename Unit>
  struct Line {
    Vec2<Unit> start, end;
  };

  using IVec2 = Vec2<int>;
  using FVec2 = Vec2<float>;
  using IRect = Rect<int>;
  using FRect = Rect<float>;
  using ILine = Line<int>;
  using FLine = Line<float>;

}

#endif //GEOM_HEADER_HXX
